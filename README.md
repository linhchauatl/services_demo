## Services Demo

This is the web-based version of [this discussion about No instance with class methods vs. Singleton design pattern](https://github.com/linhchauatl/ruby_threads).

It is written in Rails 5.1.4, running with Ruby 2.3.
You can clone this repository to your local machine (`git clone https://github.com/linhchauatl/services_demo.git`) and run `rails s` to test.

#### I. Test [No instance with class methods](https://github.com/linhchauatl/services_demo/blob/master/app/services/no_instance_service.rb)

Open two different browsers, for example Chrome and Firefox, browse to this URL at the same time:
```
http://localhost:3000/test_services/no_instance_service
```

You will see two output similar to this:

Browser 1:
```
Start NoInstanceService.execute Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b: 2018-01-15 22:25:14 -0600
End NoInstanceService.execute Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b: 2018-01-15 22:25:19 -0600

***** client in thread 'Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b' value = <Client for Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b>
```

Browser 2:
```
Start NoInstanceService.execute Request efa62523-8639-493f-bae0-e25a6f544736: 2018-01-15 22:25:16 -0600
End NoInstanceService.execute Request efa62523-8639-493f-bae0-e25a6f544736: 2018-01-15 22:25:21 -0600

***** client in thread 'Request efa62523-8639-493f-bae0-e25a6f544736' value = <Client for Request efa62523-8639-493f-bae0-e25a6f544736>
```


Please notice that the execution times of the two browser are overlaps, i.e. the executions are truly in parallel.

And notice the output lines with `*****`:
```
Browser 1:
***** client in thread 'Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b' value = <Client for Request 57580b77-97b7-4c28-8e01-ae9fd2f96a4b>

Browser 2:
***** client in thread 'Request efa62523-8639-493f-bae0-e25a6f544736' value = <Client for Request efa62523-8639-493f-bae0-e25a6f544736>
```


**The values of the clients are shown CORRECTLY in different threads.**


#### II. Test [Singleton design pattern](https://github.com/linhchauatl/services_demo/blob/master/app/services/singleton_service.rb)

Open two different browsers, for example Chrome and Firefox, browse to this URL at the same time:
```
http://localhost:3000/test_services/singleton_service
```

You will see two output similar to this:

Browser 1:
```
Start instance.execute Request b878fca5-d854-4cda-9fbc-839c2351a379: 2018-01-15 22:16:14 -0600
End instance.execute Request b878fca5-d854-4cda-9fbc-839c2351a379: 2018-01-15 22:16:19 -0600

***** @client in thread 'Request b878fca5-d854-4cda-9fbc-839c2351a379' value = <Client for Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c>
```

Browser 2:
```
Start instance.execute Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c: 2018-01-15 22:16:16 -0600
End instance.execute Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c: 2018-01-15 22:16:21 -0600

***** @client in thread 'Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c' value = <Client for Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c>
```


Please notice that the execution times of the two browser are overlaps, i.e. the executions are truly in parallel.

And here is the trouble: notice the output lines with `*****`:
```
Browser 1:
***** @client in thread 'Request b878fca5-d854-4cda-9fbc-839c2351a379' value = <Client for Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c>

Browser 2:
***** @client in thread 'Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c' value = <Client for Request d83345a4-eecb-44e6-9ebd-fc22bff04b5c>
```


**The values of the client is shown INCORRECTLY in Browser 1.**

The reason is that the server thread <2> that serves the Browser 2 was started shortly after the server thread <1> that served the Browser 1 was started. During the thread <2> execution, it updated the value of the instance variable @client of the singleton instance of class `SingletonService`. <br/>
Then when the server thread <1> rendered the value of @client, it was already updated by server thread <2>.

More details about concurrent threads, mutex lock and thread-safe can be found [here](https://github.com/linhchauatl/ruby_threads/blob/master/README.md)

**When you implement a Service using Singleton Design Pattern in a concurrent environment, you must either:**

- Use `mutex lock` to ensure all the mutable instance variables are thread-safe, and handle dead lock conditions correctly.

or

- In your shared instance, there must be no mutable instance variable.


**Food for thought:**

1. Reimplement the [Singleton design pattern in singleton_service.rb](https://github.com/linhchauatl/services_demo/blob/master/app/services/singleton_service.rb) using mutex lock, to make the output of the lines with `*****` displayed correctly for both browsers concurrently.

2. Identify all the dead lock conditions in the use of mutex lock above, and how to handle them correctly.




