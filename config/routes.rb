Rails.application.routes.draw do
  get '/test_services/no_instance_service' => 'test_services#no_instance_service'
  get '/test_services/singleton_service'   => 'test_services#singleton_service'
end
