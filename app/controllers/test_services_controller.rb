class TestServicesController < ApplicationController
  def no_instance_service
    render plain: NoInstanceService.execute("Request #{request.request_id}").join("\n")
  end

  def singleton_service
    render plain: SingletonService.instance.execute("Request #{request.request_id}").join("\n")
  end
end
