require 'singleton'

class SingletonService
  include Singleton

  def execute(execute_name)
    # This is equivalent to the case when you use a connection/client as @instance variable to read/write data
    # The data/state of the @ connection/client is changed by the read/write data operations.
    @client = "<Client for #{execute_name}>"
    output = []
    output << "Start instance.execute #{execute_name}: #{Time.now}"
    sleep 5
    output << "End instance.execute #{execute_name}: #{Time.now}\n"

    # During concurrent-thread executions, if one thread changes the value of @client
    # The one that already set in another thread is also changed, because they are the same instance
    output << "***** @client in thread '#{execute_name}' value = #{@client}"
    output
  end
end