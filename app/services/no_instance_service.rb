class NoInstanceService
  class << self
    def execute(execute_name)
      # This is equivalent to the case when you use a connection/client as local variable to read/write data
      # The data/state of the connection/client is changed by the read/write data operations.
      client = "<Client for #{execute_name}>"
      output = []
      output << "Start NoInstanceService.execute #{execute_name}: #{Time.now}"
      sleep 5
      output << "End NoInstanceService.execute #{execute_name}: #{Time.now}\n"

      # During concurrent-thread executions, if one thread changes the value of client
      # nothing will happen for the client in other threads, because they share nothing, no state.
      output << "***** client in thread '#{execute_name}' value = #{client}"
      output
    end
  end
end